@include('templates/admin')
    <div class="main cc">  

        <div class="panel panel-info">
              <div class="panel-heading clearfix">
                <h3 class="panel-title">Users Table</h3>
                <div class="btn-group fiet">
                  <a href="" class="sbm dropdown-toggle" class="" type="button" data-toggle="dropdown" aria-expanded="false">
                    <span class="octicon octicon-three-bars"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li><a href="#">create a user</a></li>
                    <li><a href="#">block users</a></li>
                    <li><a href="#">Logout users</a></li>
                    <li><a href="#">download user list</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Delete</a></li>
                  </ul>
                </div>
              </div>
              <div class="panel-body">
                <div class="error">
                 @include('_partials.errors')
                </div>
				<div class="table-responsive"> 
				<table class="table table-condensed"> 

				</table>
				</div>
              </div>
              <hr>
              <div class="line">  
                  <a href="#create_user" data-toggle="modal" data-target="#create_user"><span class="octicon octicon-plus"></span></a> |
                  <a href="#"><span class="octicon octicon-checklist"></span></a> |
                  <a href="#"><span class="octicon octicon-issue-reopened"></span></a> |
                  <a href="#"><span class="octicon octicon-lock"></span></a> |
                  <a href="#"><span class="octicon octicon-database"></span></a> 
              </div>  
            </div>                
            @include('_partials/data/addNewUser')
        
    </div> <!-- main cc -->
@include('templates/admin_end')