@include('inc/doc')
<body class="dashboard">
	@yield('header')
	@yield('sidebar')
	@yield('container')
	@section('footer')
		@parent
	@stop
</body>
</html>
