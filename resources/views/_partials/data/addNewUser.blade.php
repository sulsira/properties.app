<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="create_user" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Create a new user</h4>
      </div>
      {!!Form::open(array('route'=>'users.store','files' => true, 'class'=>"form-inline",'id'=>"user_form"))!!}
      <div class="modal-body">
          <div class="form-group">
            <label class="control-label">
              Login Details:
            </label>
            <hr>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Email </div>
                  <input  placeholder="Enter email address" name="email" type="text" class="form-control">
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Password </div>
                  <input  placeholder="Enter new password" name="password" type="password" class="form-control">
                </div>            
              </div>
            </div>
          <div class="form-group">
            <label class="control-label">
              Roles:
            </label>
            <hr>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Domain </div>
                  <select name="domain" id="accountType" class="form-control" >
                    <option value="Admin">Administration</option>
                    <option value="Estate">Reals Estate</option>
                    <option value="Rent">Rent</option>
                    <option value="Accounts">Accounts</option>
                  </select>
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Security Level </div>
                  <input  placeholder="name here" name="security_level" type="number" class="form-control" max="8" min="1" step="1">
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">User Group </div>
                  <select name="user_group" id="userGroup" class="form-control" >
                    <option value="Staff">Staff</option>
                    <option value="Agent">Agent</option>
                    <option value="Accountant">Accountant</option>
                    <option value="Manager">Manager</option>
                    <option value="Other">Other</option>
                </select>
                </div>            
              </div>
            </div>
          <div class="form-group">
            <label class="control-label">
              User Properties:
            </label>
            <hr>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Firstname </div>
                  <input  placeholder="name here" name="first_name" type="text" class="form-control">
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Last name </div>
                  <input  placeholder="name here" name="last_name" type="text" class="form-control">
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Department </div>
                  <select name="department" id="department" class="form-control" >
                    <option value="Admin">Administration</option>
                    <option value="Estate">Reals Estate</option>
                    <option value="Rent">rent</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Accounts">Accounts</option>
                  </select>

                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Phone #</div>
                  <input  placeholder="name here" name="phones" type="text" class="form-control">
                </div>            
              </div>
              <div class="sek">
                <div class="input-group">
                  <div class="input-group-addon">Photo</div>
                  <input placeholder="name here" name="profilePic" type="file" class="form-control">
                </div>            
              </div>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create User</button>
      </div>
     {!!Form::close()!!}


    </div>
  </div>
</div>
