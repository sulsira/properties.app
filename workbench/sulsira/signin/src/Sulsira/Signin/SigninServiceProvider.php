<?php namespace Sulsira\Signin;

use Illuminate\Support\ServiceProvider;

class SigninServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->package('sulsira/signin');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}
    /**
     *
     * @return void
     */
    public function boot(){
//        $this->loadTranslationsFrom(__DIR__.'/path/to/translations', 'courier');
        $this->package('sulsira/signin');
        AliasLoader::getInstance()->alias('Menu', 'Sulsira\Menu\Menu');
    }


}
