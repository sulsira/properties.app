<?php
// use Realestate/Traits/Admin;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|colo
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// $r = new  Sulsira\Accounts\Rent;
//dd($r->payment([
//    'payment' => 400,
//    'month_fee' => 100,
//    'balance'=> 300,
//    'last_paid_date' => '2015-1-31',
//    'current_payment_date' => date("Y-m-d", time()),
//    'previouly_assigned_payment_date'=> '2015-4-30'
//
//]));
//;
//
//dd(
//   Sulsira\Accounts\Mortgage::pay([
//       'amount_paid' => 500 + 500,
//       'total_paid' => 0,
//       'payment_due'=> 0,
//       'payment_duration_in_months'=> 30,
//       'calculated_monthly_fee' => 0,
//       'number_of_months_paid' => 0,
//       'unhindered_closing_payment_date'=> null,
//       'next_payment_date' => null,
//       'commencement_date' => '2015-7-26',
//       'payment_type' => null,
//       'down_payment' => 5000,
//       'plot_price' => 75000,
//       'paid_date' => date("Y-m-d", time()),
//       'arrears' => null,
//       'balance' => null
//   ])->getPayment()
//);




Route::get('/','WelcomeController@index');

Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('home', 'HomeController@index');
Route::post('login', 'Auth\AuthController@postLogin');
Route::controllers([
	'password' => 'Auth\PasswordController'
]);
Route::resource('admin', 'AdminController');
Route::resource('users', 'UsersController');