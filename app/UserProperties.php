<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class UserProperties extends Model {

	protected $table = "userProperties";
	protected $primaryKey = "id";
	protected $fillable = [
							'user_id',
							'role_id',
							'department_id',
							'contact_id',
							'person_id',
							'address_id',
							'department_id'
						];

	public function user(){
		return $this->belongsTo('User','user_id','id');
	}

}
