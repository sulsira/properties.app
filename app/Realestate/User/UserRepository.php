<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 4/15/2015
 * Time: 4:15 PM
 */

namespace Realestate\User;

use App\User;
use App\UserRole;

class UserRepository {
    public function save(User $user){
        return $user->save();
    }

    public function role(UserRole $role){
        return $role->save();
    }
} 